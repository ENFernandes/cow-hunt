package org.academiadecodigo.argicultores.Field;
import org.academiadecodigo.argicultores.Cows.Cow;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Background extends Picture{
    private Picture fBackground=new Picture(PADDING,PADDING,"resources/background-teste1.png");;
    private Picture sBackground;
    final static int PADDING = 10;

    public Background()
    {
        fBackground.draw();
    }
    public Picture getfBackground() {
        return fBackground;
    }

    public void setfBackground(Picture fBackground) {
        this.fBackground = fBackground;
    }


}

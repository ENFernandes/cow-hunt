package org.academiadecodigo.argicultores;
import org.academiadecodigo.argicultores.Cows.BigCow;
import org.academiadecodigo.argicultores.Cows.Cow;
import org.academiadecodigo.argicultores.Cows.CowFactory;
import org.academiadecodigo.argicultores.Field.Background;
import java.util.*;

public class Game {

    private Background background;
    private Cow[] cow=new Cow[3];

    private boolean condition =true;
    private final static int PADDING = 10;
    private int cont=0;

    public Game(){
    }

    public void init(){
        background = new Background();
        for (int i = 0; i < cow.length ; i++) {
            cow[i]=CowFactory.getNewCow();
        }

    }

    public void start() throws InterruptedException {
        for (Cow c : cow) {
            c.getCow().draw();
        }
        while(true) {
            Thread.sleep(10);
            moveAll();

            /*for (Cow c:cow) {
                if(c.isDead()){
                    if(!c.isVerify()){
                        c.setVerify(true);
                        cont++;
                    }
                }
            }*/
        }
    }

    public void moveAll() {
        for (Cow c : cow) {
            c.move();
                /*if (!checkBorders(c)) {
                    c.direction(true);
                    Thread.sleep(30);
                } else {
                    c.direction(false);
                    Thread.sleep(30);
                    if(c.isDead() && c.getCont()>=3);
                }*/
        }
    }
    /*public boolean checkBorders(Cow cow) {
        int cowX = cow.getCow().getX();
        int cowY = cow.getCow().getY();
        int backgroundX = background.getfBackground().getX();
        int backgroundY = background.getfBackground().getY();

        if (cowX+ PADDING < (backgroundX + PADDING) ||
                cowX + cow.getCow().getWidth() > background.getfBackground().getWidth() ||
                cowY + PADDING < backgroundY + PADDING ||
                cowY > background.getfBackground().getHeight()) {
            return true;
        }
        return false;
    }*/
}

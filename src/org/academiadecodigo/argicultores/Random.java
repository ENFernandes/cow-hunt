package org.academiadecodigo.argicultores;

public class Random {
    private int random;
    public Random(int num) {
        random = (int) (Math.floor(Math.random() * num));
    }
    public int getRandom() {
        return random;
    }
}

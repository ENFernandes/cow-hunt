package org.academiadecodigo.argicultores.Cows;

import org.academiadecodigo.argicultores.Random;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class SmallCow extends Cow {
    public SmallCow() {
        super(CowType.SMALLCOW, 1);
        Random ran = new Random(800);
        setCow(new Picture(400, ran.getRandom(), "resources/cow_small.png"));
    }

    @Override
    public void move() {
        direction();
    }
}

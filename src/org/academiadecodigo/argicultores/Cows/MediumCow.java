package org.academiadecodigo.argicultores.Cows;

import org.academiadecodigo.argicultores.Random;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class MediumCow extends Cow {
    public MediumCow() {
        super(CowType.MEDIUMCOW, 1);
        Random ran = new Random(800);
        setCow(new Picture(400, ran.getRandom(), "resources/cow_medium.png"));
    }
    @Override
    public void move() {
        direction();
    }
}

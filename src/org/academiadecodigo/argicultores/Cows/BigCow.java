package org.academiadecodigo.argicultores.Cows;
import org.academiadecodigo.argicultores.Random;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class BigCow extends Cow {

    public BigCow() {
        super(CowType.BIGCOW, 1);
        Random ran = new Random(800);
        setCow(new Picture(400, ran.getRandom(), "resources/cow_big.png"));
    }

    @Override
    public void move() {
        direction();
    }
}


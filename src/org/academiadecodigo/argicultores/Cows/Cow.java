package org.academiadecodigo.argicultores.Cows;
import org.academiadecodigo.argicultores.Field.Direction;
import org.academiadecodigo.argicultores.Random;
import org.academiadecodigo.simplegraphics.pictures.Picture;


abstract public class Cow extends Picture {


    private CowType cowType;
    private int life;
    private int directionChangeLevel = 8;
    private Direction currentDirection;
    private int speed;
    private boolean isDead=false;
    private boolean verify=false;
    private Picture cow;
    private int cont = 0;

    public Cow(CowType cowType,int speed) {
        this.speed = speed;
        this.cowType = cowType;
        currentDirection = Direction.values()[(int) (Math.random() * Direction.values().length)];
    }





    public int getCont() {
        return cont;
    }
    public boolean isVerify() {
        return verify;
    }
    public void setVerify(boolean verify) {
        this.verify = verify;
    }
    public int getLife() {
        return life;
    }
    public int getSpeed() {
        return speed;
    }
    public boolean isDead() {
        return isDead;
    }
    public void setDead(boolean dead) {
        isDead = dead;
    }
    public void drawCow(Picture cow) {
        cow.draw();
    }
    public void deleteCow(Picture cow) {
        cow.delete();
    }

    public Picture getCow() {
        return cow;
    }
    public void setCow(Picture cow){
        this.cow = cow;
    }

    abstract public void move();

    public void direction() {

        Random random = new Random(100);
        int rand = random.getRandom();
        if(rand < 10)
        {
            currentDirection = Direction.values()[(int) (Math.random() * Direction.values().length)];
        }
        switch (currentDirection) {
            case RIGHT://direita
                cow.translate(1, 0);
                break;
            case LEFT://esquerda
                cow.translate(-1, 0);
                break;
            case UP://cima
                cow.translate(0, -1);
                break;
        }
    }
}
